// editing image via css properties
function editImage() {
	
	
	
	var br 		 = $("#br").val();      // brightness
	var ct 		 = $("#ct").val();      // contrast
	var huer	 = $("#huer").val();    //hue-rotate
	var opacity  = $("#opacity").val(); //opacity
	var saturate = $("#saturate").val();//saturate

	var filter = 	'brightness(' + br +
			'%) contrast(' + ct +
			'%) hue-rotate(' + huer +
			'deg) opacity(' + opacity +
			'%) saturate(' + saturate +
			'%)';

    var bright 	= 'brightness(' + br +'%)'// brightness value
	var cont 	= 'contrast(' + ct +'%)'// contrast value
	var huerot	= 'hue-rotate(' + huer +'%)'// hue-rotate value
	var opac    = 'opacity(' + opacity +'%)'// opacity value
	var satur   = 'saturate(' + saturate +'%)'// saturate value

	$("#imageContainer video").css("filter", filter);
	$("#imageContainer video").css("-webkit-filter", filter); 

}

//When sliders change image will be updated via editImage() function
$("input[type=range]").change(editImage).mousemove(editImage);

// Reset sliders back to their original values on press of 'reset'
$('#imageEditor').on('reset', function () {
	setTimeout(function() {
		editImage();
	},0);
});